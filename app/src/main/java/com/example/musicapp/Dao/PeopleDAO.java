package com.example.musicapp.Dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.example.musicapp.Models.People;

@Dao
public interface PeopleDAO {
    @Insert
    long insertPeople(People people);
}
