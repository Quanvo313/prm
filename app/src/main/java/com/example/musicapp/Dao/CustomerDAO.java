package com.example.musicapp.Dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.example.musicapp.Models.Customer;

@Dao
public interface CustomerDAO {
    @Insert
    void insertCustomer(Customer customer);
}
