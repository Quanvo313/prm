package com.example.musicapp.AdapterController;

public interface BillListener {
    void onViewBillDetail(int billID);
}
