package com.example.musicapp.AdapterController;

import com.example.musicapp.Models.Product;

public interface ProductListener {
    void onUpdateProduct(Product product);
    void onDeleteProduct(int id, int pos);

    void onViewDetailProduct(Product product);
}
