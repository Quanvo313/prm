package com.example.musicapp.Models;

public enum Role {
    CUSTOMER,
    ADMIN,
    STAFF
}
