package com.example.musicapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.musicapp.Dao.AccountDAO;
import com.example.musicapp.Dao.BillDAO;
import com.example.musicapp.Dao.Bill_ProductDAO;
import com.example.musicapp.Dao.CartDAO;
import com.example.musicapp.Dao.Cart_ProductDAO;
import com.example.musicapp.Dao.CategoryDAO;
import com.example.musicapp.Dao.CustomerDAO;
import com.example.musicapp.Dao.PeopleDAO;
import com.example.musicapp.Dao.ProductDAO;
import com.example.musicapp.Models.Account;
import com.example.musicapp.Models.Bill;
import com.example.musicapp.Models.Bill_Product;
import com.example.musicapp.Models.Cart;
import com.example.musicapp.Models.Cart_Product;
import com.example.musicapp.Models.Category;
import com.example.musicapp.Models.Customer;
import com.example.musicapp.Models.People;
import com.example.musicapp.Models.Product;

@Database(entities = {Product.class, Category.class, Account.class, People.class, Customer.class, Cart.class, Cart_Product.class, Bill.class, Bill_Product.class}, version = 38)
public abstract  class MusicDatabase extends RoomDatabase  {
    public abstract ProductDAO getProductDAO();
    public abstract CategoryDAO getCategoryDAO();
    public abstract AccountDAO getAccountDAO();
    public abstract CustomerDAO getCustomerDAO();
    public abstract PeopleDAO getPeopleDAO();
    public abstract CartDAO getCartDAO();
    public abstract Cart_ProductDAO getCart_ProductDAO();
    public abstract BillDAO getBillDAO();
    public abstract Bill_ProductDAO getBillProductDAO();

    public static MusicDatabase INSTANCE;

    public static MusicDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, MusicDatabase.class, "MusicDatabase")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
